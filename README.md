1. SafeDrive adalah sebuah aplikasi pendeteksian yang bertujuan untuk meningkatkan keselamatan pengemudi saat mengendarai mobil. Aplikasi ini menggunakan teknologi penginderaan visual dan pengenalan wajah untuk mendeteksi apakah pengemudi memiliki mata tertutup atau sedang menguap selama perjalanan. Dengan adanya SafeDrive, pengemudi dapat diberi peringatan yang tepat waktu untuk menghindari kecelakaan yang disebabkan oleh konsentrasi yang kurang atau gangguan saat mengemudi.

List Use Case Aplikasi SafeDrive:

* Deteksi Mata Tertutup: Aplikasi SafeDrive dapat secara akurat mendeteksi saat mata pengemudi tertutup selama mengemudi. Ketika mata tertutup terdeteksi, aplikasi akan memberikan peringatan suara atau tampilan visual yang mengingatkan pengemudi untuk tetap waspada dan membuka mata mereka.

* Pendeteksian Ketika Menguap: SafeDrive juga dapat mengenali gerakan khas saat pengemudi menguap. Jika aplikasi mendeteksi tanda-tanda menguap, seperti mulut terbuka secara berulang, aplikasi akan memberikan peringatan agar pengemudi dapat beristirahat sejenak dan mengembalikan fokusnya sebelum melanjutkan perjalanan.

* Pemberitahuan Kondisi Pengemudi: Aplikasi SafeDrive dapat memberikan laporan tentang perilaku pengemudi, seperti durasi mata tertutup atau berapa kali mereka menguap selama perjalanan. Fitur ini dapat membantu pengemudi untuk menyadari kebiasaan buruk yang mungkin mengganggu konsentrasi mereka saat mengemudi dan memotivasi mereka untuk mengambil tindakan yang diperlukan.


* Peringatan Suara: Saat mata tertutup atau tanda-tanda menguap terdeteksi, Asisten Kecerdasan Buatan akan memberikan peringatan suara yang mengingatkan pengemudi untuk tetap waspada. Contohnya, asisten dapat mengatakan, "Perhatian, tampaknya Anda memiliki mata tertutup. Mohon buka mata Anda dan konsentrasikan pada perjalanan."


2. SafeDriver menggunakan algoritma Haar Cascade dan shape predictor

Haar Cascade: Haar Cascade adalah sebuah algoritma deteksi objek yang menggunakan fitur-fitur visual dari objek yang ingin dideteksi. Pada kasus ini, Haar Cascade digunakan untuk mendeteksi wajah pengemudi.

Langkah-langkah algoritma Haar Cascade dalam mendeteksi wajah pengemudi adalah sebagai berikut:

a. Pra-pemrosesan: Citra dari kamera atau video diambil dan diubah menjadi citra skala abu-abu untuk mengurangi kompleksitas pemrosesan. Kemudian, citra tersebut dapat di-normalisasi atau di-equalisasi histogramnya untuk meningkatkan kontras.

b. Pembuatan Integral Image: Integral Image dibuat untuk menghitung jumlah piksel di dalam setiap sub-daerah citra. Ini mempercepat perhitungan fitur Haar.

c. Penerapan Cascades: Haar-like features (fitur-fitur mirip Haar) digunakan untuk membedakan antara wajah dan bagian lain dari citra. Ini melibatkan penggunaan filter-fitur, seperti filter kotak putih-hitam atau garis horizontal, untuk mendeteksi pola yang ada dalam wajah.

d. Deteksi Wajah: Setelah menghitung respons dari fitur-fitur Haar, metode cascading digunakan untuk menghilangkan daerah non-wajah dan mengidentifikasi wilayah yang kemungkinan besar berisi wajah.

e. Pemilihan Wilayah Minim: Hasil deteksi Haar Cascade mungkin menghasilkan beberapa deteksi wajah yang tumpang tindih. Untuk mengatasi ini, dilakukan pemilihan wilayah minimum untuk memastikan hanya satu deteksi wajah yang benar-benar mewakili wajah pengemudi.

Shape Predictor: Setelah wajah pengemudi terdeteksi menggunakan Haar Cascade, shape predictor digunakan untuk mendeteksi tanda-tanda kelelahan, seperti mata yang tertutup atau menguap. Shape predictor adalah sebuah model yang dilatih untuk mengenali berbagai titik landmark (landmarks) pada wajah manusia.

Langkah-langkah algoritma shape predictor dalam mendeteksi tanda-tanda kelelahan pada wajah pengemudi adalah sebagai berikut:

a. Training: Model shape predictor dilatih menggunakan dataset yang berisi wajah-wajah dengan berbagai posisi dan ekspresi. Titik-titik landmark pada wajah di-annotasi secara manual pada dataset ini.

b. Deteksi Titik Landmark: Setelah model shape predictor terlatih, model tersebut dapat digunakan untuk mendeteksi titik-titik landmark pada wajah pengemudi yang telah terdeteksi sebelumnya. Titik-titik landmark ini bisa mencakup posisi mata, hidung, mulut, dll.

c. Analisis Gerakan dan Kelelahan: Dengan menggunakan titik-titik landmark, dapat dilakukan analisis gerakan dan kelelahan pada wajah pengemudi. Misalnya, pengamatan terhadap gerakan mata dan perubahan posisi atau bentuk mata untuk mendeteksi jika pengemudi sedang menguap atau mata tertutup dalam waktu yang lama.

d. Pemberian Peringatan: Jika tanda-tanda kelelahan terdeteksi, asisten SafeDriver memberikan peringatan kepada pengemudi agar tetap fokus dan waspada, mungkin dalam bentuk suara, getaran, atau tampilan visual.

3. https://github.com/fikri-taufiqurrahman/safeDrive/tree/main

4. https://youtu.be/5IrqI9jwsD0

6. safeDrive adalah sebuah aplikasi pendeteksian yang bertujuan untuk meningkatkan keselamatan pengemudi saat mengendarai mobil. Aplikasi ini menggunakan teknologi penginderaan visual dan pengenalan wajah untuk mendeteksi apakah pengemudi memiliki mata tertutup atau sedang menguap selama perjalanan. Dengan adanya SafeDrive, pengemudi dapat diberi peringatan yang tepat waktu untuk menghindari kecelakaan yang disebabkan oleh konsentrasi yang kurang atau gangguan saat mengemudi.

List Use Case Aplikasi SafeDrive:

* Deteksi Mata Tertutup: Aplikasi SafeDrive dapat secara akurat mendeteksi saat mata pengemudi tertutup selama mengemudi. Ketika mata tertutup terdeteksi, aplikasi akan memberikan peringatan suara atau tampilan visual yang mengingatkan pengemudi untuk tetap waspada dan membuka mata mereka.

* Pendeteksian Ketika Menguap: SafeDrive juga dapat mengenali gerakan khas saat pengemudi menguap. Jika aplikasi mendeteksi tanda-tanda menguap, seperti mulut terbuka secara berulang, aplikasi akan memberikan peringatan agar pengemudi dapat beristirahat sejenak dan mengembalikan fokusnya sebelum melanjutkan perjalanan.

* Pemberitahuan Kondisi Pengemudi: Aplikasi SafeDrive dapat memberikan laporan tentang perilaku pengemudi, seperti durasi mata tertutup atau berapa kali mereka menguap selama perjalanan. Fitur ini dapat membantu pengemudi untuk menyadari kebiasaan buruk yang mungkin mengganggu konsentrasi mereka saat mengemudi dan memotivasi mereka untuk mengambil tindakan yang diperlukan.


* Peringatan Suara: Saat mata tertutup atau tanda-tanda menguap terdeteksi, Asisten Kecerdasan Buatan akan memberikan peringatan suara yang mengingatkan pengemudi untuk tetap waspada. Contohnya, asisten dapat mengatakan, "Perhatian, tampaknya Anda memiliki mata tertutup. Mohon buka mata Anda dan konsentrasikan pada perjalanan."

7. https://github.com/fikri-taufiqurrahman/safeDrive/tree/main

8. https://youtu.be/5IrqI9jwsD0

10. https://docs.google.com/document/d/1zbaoquBQdm0eNWTWPAEM2wkFcQb7wXQq6-650UbwlZU